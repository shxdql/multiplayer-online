﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameController : MonoBehaviour
{
    [SerializeField] GameObject gameManager;

    private void Start()
    {
        PhotonNetwork.Instantiate(gameManager.name, Vector3.zero, Quaternion.identity);
    }
}
