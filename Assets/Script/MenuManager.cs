﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public static MenuManager mm;

    [SerializeField] GameObject[] panel;

    private void Awake()
    {
        mm = this;
    }

    public void OpenPanel(string pan)
    {
        for (int i = 0; i < panel.Length; i++)
        {
            if (panel[i].name != pan)
                panel[i].SetActive(false);
            else
                panel[i].SetActive(true);
        }
    }
}
