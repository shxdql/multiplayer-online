﻿using SimpleInputNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject playerPrefab;

    PhotonView pv;
    Text nameText;

    private void Awake()
    {
        pv = GetComponent<PhotonView>();
    }

    private void Start()
    {
        if (pv.IsMine)
        {
            CreateChar();
            nameText = GameObject.Find("PlayerNameText").GetComponent<Text>();
            nameText.text = PhotonNetwork.NickName;
        }
    }

    void CreateChar()
    {
        PhotonNetwork.Instantiate(playerPrefab.name, new Vector2(Random.Range(-17f, 17f), -9f), Quaternion.identity);
    }
}
