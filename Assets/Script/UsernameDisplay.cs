﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsernameDisplay : MonoBehaviour
{
    PhotonView playerPV;

    Text username;

    private void Start()
    {
        username = GetComponentInChildren<Text>();
        playerPV = GetComponent<PhotonView>();
        username.text = playerPV.Owner.NickName;
    }
}
