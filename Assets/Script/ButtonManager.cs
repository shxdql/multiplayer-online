﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class ButtonManager : MonoBehaviour
{
    bool isRun;

    private void Update()
    {
        if (FindObjectOfType<PlayerScript>() != null)
        {
            if (isRun)
            {
                PlayerScript.ps.speed = 50f;
            }
            else
            {
                PlayerScript.ps.speed = 25f;
            }
        }
    }

    public void Run()
    {
        if (!isRun)
            isRun = true;
        else
            isRun = false;
    }

    public void BackToMenu()
    {
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Main");
    }
}
