﻿using SimpleInputNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript ps;

    [HideInInspector] public float speed;

    Joystick axis;
    Rigidbody2D rb;
    PhotonView pv;
    Animator anim;

    private void Awake()
    {
        ps = this;
        pv = GetComponent<PhotonView>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        axis = FindObjectOfType<Joystick>();
    }

    private void Start()
    {
        pv.Owner.SetScore(0);
    }

    private void Update()
    {
        if (pv.IsMine)
            Move();
    }

    void Move()
    {
        rb.velocity = new Vector2(axis.xAxis.value, axis.yAxis.value) * speed * 15 * Time.deltaTime;

        anim.SetFloat("MoveX", axis.xAxis.value);
        anim.SetFloat("MoveY", axis.yAxis.value);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Quest" && pv.IsMine)
        {
            pv.RPC("RPC_Scoring", RpcTarget.AllBuffered);

            if (!pv.Owner.IsMasterClient)
                pv.RPC("RPC_Destroy", RpcTarget.AllBuffered, collision.gameObject.GetComponent<PhotonView>().ViewID);
            else
                PhotonNetwork.Destroy(collision.gameObject);
        }
    }

    [PunRPC]
    public void RPC_Scoring()
    {
        pv.Owner.AddScore(1);
    }

    [PunRPC]
    public void RPC_Destroy(int viewID)
    {
        PhotonNetwork.Destroy(PhotonView.Find(viewID).gameObject);
    }

    public void UpdateScore(UnityEngine.UI.Text text)
    {
        text.text = PhotonNetwork.LocalPlayer.GetScore().ToString();
    }
}
