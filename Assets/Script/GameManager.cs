﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public static GameManager gm;

    Text scoreText;
    PhotonView pv;

    private void Awake()
    {
        gm = this;
        pv = GetComponent<PhotonView>();
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
    }

    private void Update()
    {
        if (FindObjectOfType<PlayerScript>() != null)
        {
            if (pv.IsMine)
                PlayerScript.ps.UpdateScore(scoreText);
        }
    }
}
