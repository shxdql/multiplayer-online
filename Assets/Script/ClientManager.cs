﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ClientManager : MonoBehaviourPunCallbacks
{
    [SerializeField] InputField nameInput, roomNameInput;
    [SerializeField] Button createRoom, findRoom, startGame;
    [SerializeField] Text mess;

    private void Update()
    {
        if (string.IsNullOrEmpty(nameInput.text) || string.IsNullOrEmpty(roomNameInput.text))
        {
            createRoom.interactable = findRoom.interactable = false;
        }
        else
        {
            createRoom.interactable = findRoom.interactable = true;
        }
    }

    private void Start()
    {
        MenuManager.mm.OpenPanel("Loading");
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnJoinedLobby()
    {
        MenuManager.mm.OpenPanel("Menu");
    }

    public override void OnJoinedRoom()
    {
        MenuManager.mm.OpenPanel("Room");

        if (!PhotonNetwork.IsMasterClient)
        {
            startGame.gameObject.SetActive(false);
            mess.text = "Waiting Host Room to Start the Game . . .";
        }
        else
            mess.text = "You are the Host Room";
    }

    public override void OnLeftRoom()
    {
        MenuManager.mm.OpenPanel("Menu");
    }

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(roomNameInput.text);
        PhotonNetwork.NickName = nameInput.text;
        MenuManager.mm.OpenPanel("Loading");
    }

    public void FindRoom()
    {
        PhotonNetwork.JoinRoom(roomNameInput.text);
        PhotonNetwork.NickName = nameInput.text;
        MenuManager.mm.OpenPanel("Loading");
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        MenuManager.mm.OpenPanel("Loading");
    }

    public void StartGame()
    {
        PhotonNetwork.LoadLevel("GameScene");
    }
}
