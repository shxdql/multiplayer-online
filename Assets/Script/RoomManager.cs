﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using System;

public class RoomManager : MonoBehaviour
{
    public static RoomManager rm;

    [SerializeField] GameObject playerController;
    [SerializeField] GameObject endGamePanel;
    [SerializeField] Text winner;

    private void Awake()
    {
        rm = this;
    }

    private void Start()
    {
        PhotonNetwork.Instantiate(playerController.name, Vector3.zero, Quaternion.identity);
        endGamePanel.SetActive(false);
    }

    private void Update()
    {
        CheckTotalScore();
    }

    void CheckTotalScore()
    {
        int total = 0;

        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            total += PhotonNetwork.PlayerList[i].GetScore();
        }

        if (total == 7)
        {
            StartCoroutine(EndGame());
        }
    }

    IEnumerator EndGame()
    {
        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            for (int j = 1; j < PhotonNetwork.PlayerList.Length; j++)
            {
                if (PhotonNetwork.PlayerList[i].GetScore() < PhotonNetwork.PlayerList[j].GetScore())
                    PhotonNetwork.PlayerList[0] = PhotonNetwork.PlayerList[j];
            }
        }

        endGamePanel.SetActive(true);
        winner.text = PhotonNetwork.PlayerList[0].NickName + " win!";
        yield return new WaitForSeconds(3);
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Main");
    }
}
